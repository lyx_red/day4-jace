# ORID



## Objective

- Today, I learned about Unit Testing and TDD, and did some practical exercises. In the course of learning Unit Testing, I learned what Unit Testing is, why we need it, and how to write and run Unit Tests. In the course of TDD, I learned how to write code based on tests, determine the implementation of the code by writing tests first, and make the tests part of the code. The quality and maintainability of the code can be better guaranteed through such a test-based development method. In the practical exercise, I wrote some simple test cases in Java and ran the tests to verify the correctness of the code. In conclusion, today's learning has given me a deeper understanding of the importance of testing in software development, and how to ensure the quality and maintainability of code through testing.



## Reflective

- I feel excited and challenged

## Interpretative

- I feel very excited to learn new knowledge and these new knowledge points are very useful for my future work. At the same time, I think it is very challenging for me to truly master TDD and skillfully use the idea of TDD.


## Decision

I will continue to learn more knowledge from the teachers in the following time. At the same time, I will continue to communicate with my friends and finish more homework together.