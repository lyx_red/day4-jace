package com.afs.tdd;

import java.util.List;

public class MarsRover {

    private final Location location;

    public Location getLocation() {
        return location;
    }

    public MarsRover(Location location) {
        this.location = location;
    }

    public void executeCommand(Command command) {
        if (command.equals(Command.Move)) {
            move();
        }else if (command.equals(Command.Left)) {
            left();
        }else {
            right();
        }
    }

    private void right() {
        Enum direction = location.getDirection();
        if (Direction.North.equals(direction)) {
            location.setDirection(Direction.East);
        } else if(Direction.South.equals(direction)) {
            location.setDirection(Direction.West);
        } else if (Direction.East.equals(direction)) {
            location.setDirection(Direction.South);
        } else {
            location.setDirection(Direction.North);
        }
    }

    private void left() {
        Enum direction = location.getDirection();
        if (Direction.North.equals(direction)) {
            location.setDirection(Direction.West);
        } else if (Direction.South.equals(direction)) {
            location.setDirection(Direction.East);
        } else if (Direction.East.equals(direction)) {
            location.setDirection(Direction.North);
        } else {
            location.setDirection(Direction.South);
        }
    }

    private void move() {
        Enum direction = location.getDirection();
        if (direction.equals(Direction.North)) {
            location.setCoordinateY(location.getCoordinateY() + 1);
        } else if (direction.equals(Direction.South)) {
            location.setCoordinateY(location.getCoordinateY() - 1);
        } else if (direction.equals(Direction.East)) {
            location.setCoordinateX(location.getCoordinateX() + 1);
        } else {
            location.setCoordinateX(location.getCoordinateX() - 1);
        }

    }

    public void executeBatchCommands(List<Command> commandList) {
        commandList.forEach(command -> executeCommand(command));
    }
}
