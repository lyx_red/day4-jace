package com.afs.tdd;

import java.util.Objects;

public class Location {
    private int coordinateX;
    private int coordinateY;
    private Enum direction;

    public Location(int coordinateX, int coordinateY, Enum direction) {
        this.coordinateX = coordinateX;
        this.coordinateY = coordinateY;
        this.direction = direction;
    }

    public int getCoordinateX() {
        return coordinateX;
    }

    public void setCoordinateX(int coordinateX) {
        this.coordinateX = coordinateX;
    }

    public int getCoordinateY() {
        return coordinateY;
    }

    public void setCoordinateY(int coordinateY) {
        this.coordinateY = coordinateY;
    }

    public Enum getDirection() {
        return direction;
    }

    public void setDirection(Enum direction) {
        this.direction = direction;
    }
}
